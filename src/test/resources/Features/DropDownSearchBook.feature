Feature: feature to test dropdownlist and search book menu functionality

  @DropDownListFeature
  Scenario: Check input dropdownlist is successful
    Given User go to "https://demoqa.com/select-menu"
    When User in “select menu” page
    And User choose select value "Another root option"
    And User choose select one "Other"
    And User choose old style select menu "Aqua"
    And User choose multi select drop down "all color"
    Then User success input all select menu

  @SearchBookScenarioFail
  Scenario: Check searching the book in "Book Store" page and result not found
    Given User go to "https://demoqa.com/books"
    When User in "Book Store" page
    And User search book "qa engineer"
    Then User see "No rows found"

  @SearchBookScenarioSuccess
  Scenario: Check searching the book in "Book Store" Page and success
    Given User go to "https://demoqa.com/books"
    When User in "Book Store" page
    And User search book "Git Pocket Guide"
    And User click book "Git Pocket Guide"
    Then User see "Git Pocket Guide"



