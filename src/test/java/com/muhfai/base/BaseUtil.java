package com.muhfai.base;

import com.muhfai.pages.DropDownListPage;
import com.muhfai.pages.SearchBookPage;
import org.openqa.selenium.WebDriver;

public class BaseUtil {
    public WebDriver driver;
    public DropDownListPage dropDownListPage;
    public SearchBookPage searchBookPage;
}
