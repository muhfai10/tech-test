package com.muhfai.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.*;

import java.time.Duration;
import java.util.List;

public class DropDownListPage {

    WebDriver driver;

    By byInputSelectValue  = By.xpath("//body[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[2]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/input[1]");

    By dropDownSelectValue = By.xpath("//body[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[2]/div[2]/div[1]/div[1]/div[1]");
    By dropDownSelectOne = By.xpath("//body/div[@id='app']/div[1]/div[1]/div[2]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]");

    By byInputSelectOne = By.xpath("//body[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/input[1]");
    By dropDownOldSelect = By.xpath("//select[@id='oldSelectMenu']");

    By dropDownMultiSelect = By.cssSelector("div.body-height:nth-child(2) div.container.playgound-body div.row div.col-12.mt-4.col-md-6:nth-child(2) div.select-menu-container:nth-child(2) div.row:nth-child(7) div.col-md-6.col-sm-12 > div.css-2b097c-container:nth-child(4)");

    By byInputMultiSelect = By.cssSelector("#react-select-4-input");
    By textSelectPage = By.xpath("//div[contains(text(),'Select Value')]");


    public DropDownListPage(WebDriver driver) {
        this.driver = driver;
    }

    public void checkTextSelectPageExist(){
        Wait<WebDriver> fluentWait = new FluentWait<WebDriver>(driver)
                .withTimeout(Duration.ofSeconds(10))
                .pollingEvery(Duration.ofSeconds(1))
                .ignoring(org.openqa.selenium.NoSuchElementException.class);
        fluentWait.until(ExpectedConditions.visibilityOfElementLocated(textSelectPage));
        System.out.println("Waiting Success..");
    }

    public void pickSelectValue(String value){

        driver.findElement(dropDownSelectValue).click();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        WebElement inputSelectValue = wait.until(ExpectedConditions.visibilityOfElementLocated(byInputSelectValue));
        inputSelectValue.sendKeys(value + Keys.ENTER);
        //driver.findElement(By.xpath("//div[contains(text(),'" + value +"')]")).click();
        System.out.println("pickSelectValue..");

    }

    public void pickSelectOne(String value){

        driver.findElement(dropDownSelectOne).click();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        WebElement inputSelectOne = wait.until(ExpectedConditions.visibilityOfElementLocated(byInputSelectOne));
        inputSelectOne.sendKeys(value + Keys.ENTER);
        //driver.findElement(By.xpath("//div[contains(text(),'" + value +"')]")).click();
        System.out.println("pickSelectOne..");

    }

    public void pickOldSelect(String value){
        WebElement oldDropDown = driver.findElement(dropDownOldSelect);
        Select select = new Select(oldDropDown);
        select.selectByVisibleText(value);
        System.out.println("pickOldSelect..");
    }

    public void pickMultipleSelect(List<String> listString){

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.elementToBeClickable(dropDownMultiSelect)).click();
        for (String value:listString){

            WebElement inputMultiple = wait.until(ExpectedConditions.visibilityOfElementLocated(byInputMultiSelect));
            inputMultiple.sendKeys(value + Keys.ENTER);
            System.out.println("pickMultiple.. " + "(" + value + ")");
        }
        driver.findElement(By.xpath("//body/div[@id='app']/div[1]/div[1]/div[2]/div[2]/div[2]/div[7]/div[1]/div[1]/div[1]/div[2]/div[2]/*[1]")).click();
    }
}
