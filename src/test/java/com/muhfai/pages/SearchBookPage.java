package com.muhfai.pages;

import com.muhfai.model.Books;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class SearchBookPage {
    WebDriver driver;

    Books books = new Books();
    WebDriverWait wait;

    public SearchBookPage(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
    }

    By textFlagNoRows = By.xpath("//div[contains(text(),'No rows found')]");
    By textBookStore = By.xpath("//body/div[@id='app']/div[1]/div[1]/div[1]/div[1]");
    By tbSearchBook = By.xpath("//input[@id='searchBox']");
    By rowDataBook = By.xpath("//body/div[@id='app']/div[1]/div[1]/div[2]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]");

    //By rowDetailBook = By.xpath("//body/div[@id='app']/div[1]/div[1]/div[2]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]");
    public void checkSuccessLandedPage(String value){
        WebElement objectText = wait.until(ExpectedConditions.visibilityOfElementLocated(textBookStore));
        Assert.assertEquals(objectText.getText(), value);
    }

    public void searchBook(String value){

        WebElement inputSelectValue = wait.until(ExpectedConditions.visibilityOfElementLocated(tbSearchBook));
        inputSelectValue.sendKeys(value + Keys.ENTER);
    }

    public void checkUserSeeNotFound(String value){
        List<WebElement> webElementList = driver.findElements(rowDataBook);
        if (webElementList.size() > 0) {
            WebElement textNoRows = wait.until(ExpectedConditions.visibilityOfElementLocated(textFlagNoRows));
            System.out.println(textNoRows.getText() + "<==>" + value);
            Assert.assertEquals(value, textNoRows.getText());
            System.out.println("--User see no rows--");
        }

    }

    public void checkUserSeeBook(){
        String titleResult = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[contains(text(),'Git Pocket Guide')]"))).getText();
        String authorResult = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[contains(text(),'Richard E. Silverman')]"))).getText();
        String publisherResult = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html[1]/body[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[2]/div[2]/div[5]/div[2]/label[1]"))).getText();

        org.testng.Assert.assertEquals(books.getTitle(),titleResult);
        org.testng.Assert.assertEquals(books.getAuthor(), authorResult);
        org.testng.Assert.assertEquals(books.getPublisher(), publisherResult);
    }

    public void clickBook(String value){
        WebElement titleBook = wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(value)));
        WebElement authorBook = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'Richard E. Silverman')]")));
        WebElement publisherBook = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//body/div[@id='app']/div[1]/div[1]/div[2]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/div[4]")));
        books.setTitle(titleBook.getText());
        books.setAuthor(authorBook.getText());
        books.setPublisher(publisherBook.getText());

        titleBook.click();
        System.out.println("--User Click a Book--");

    }

}
