package com.muhfai.apitest;


import io.restassured.common.mapper.TypeRef;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class Beers {

    @Test
    public void testGetBeersByPerPage(){
        baseURI = "https://api.punkapi.com/v2";

        given().
                get("/beers?page=2&per_page=20").
                then().
                assertThat().
                body("size()", is(20));

        given().
                get("/beers?page=2&per_page=5").
                then().
                assertThat().
                body("size()", is(5));

        given().
                get("/beers?page=2&per_page=1").
                then().
                assertThat().
                body("size()", is(1));
    }

    @Test
    public void testGetBeers(){
        baseURI = "https://api.punkapi.com/v2";

        given().
                get("/beers").
                then().
                assertThat().
                body("size()", is(25));

        given().
                get("/beers").
                then().
                assertThat().
                body(JsonSchemaValidator.matchesJsonSchemaInClasspath("schema.json")).
                statusCode(200);

        //Output name
        List<Map<String, Object>> listResponse = given().get("/beers").then()
                .extract().body().as(new TypeRef<List<Map<String, Object>>>() {
                });
        System.out.println("Amount of Response = " + listResponse.size());
        System.out.println("All of names = ");
        for(Map<String, Object> response : listResponse){
            System.out.println(response.get("name").toString());
        }

    }
}
