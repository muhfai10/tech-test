package com.muhfai.runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/Features",
        glue = {"com.muhfai.StepDefinitions"},
        tags = "@SearchBookScenarioSuccess or @SearchBookScenarioFail or @DropDownListFeature"
)
public class TestRunner {
}
