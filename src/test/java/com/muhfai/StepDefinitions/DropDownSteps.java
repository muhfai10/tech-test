package com.muhfai.StepDefinitions;

import com.muhfai.base.BaseUtil;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import com.muhfai.pages.DropDownListPage;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class DropDownSteps extends BaseUtil{

    private BaseUtil base;

    public DropDownSteps(BaseUtil base) {
        this.base = base;
    }


    @When("User in “select menu” page")
    public void userInSelectMenuPage() {
        System.out.println("-- in Select Menu Page --");

        base.dropDownListPage.checkTextSelectPageExist();

    }

    @And("User choose select value {string}")
    public void userChooseSelectValueAnotherRootOption(String value) {
        System.out.println("-- Choose Select Value --");
        base.dropDownListPage.pickSelectValue(value);
    }

    @And("User choose select one {string}")
    public void userChooseSelectOneOther(String value) {
        System.out.println("-- Choose Select One --");
        base.dropDownListPage.pickSelectOne(value);
    }

    @And("User choose old style select menu {string}")
    public void userChooseOldStyleSelectMenuAqua(String value) {
        System.out.println("-- Choose Select Old Style --");
        base.dropDownListPage.pickOldSelect(value);

    }

    @And("User choose multi select drop down {string}")
    public void userChooseMultiSelectDropDownAllColor(String value) {
        System.out.println("-- Choose Multiselect --");
        List<String> listValue = new ArrayList<>();
        if(value.equalsIgnoreCase("all color")){
            listValue.add("Green");
            listValue.add("Blue");
            listValue.add("Black");
            listValue.add("Red");
        }else {
            listValue.add(value);
        }
        base.dropDownListPage.pickMultipleSelect(listValue);
    }

    @Then("User success input all select menu")
    public void userSuccessInputAllSelectMenu() {
        System.out.println("-- Choose Success --");
    }
}
