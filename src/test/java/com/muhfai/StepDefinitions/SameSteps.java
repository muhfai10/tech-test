package com.muhfai.StepDefinitions;

import com.muhfai.base.BaseUtil;
import com.muhfai.pages.DropDownListPage;
import com.muhfai.pages.SearchBookPage;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class SameSteps extends BaseUtil {

    private BaseUtil base;

    public SameSteps(BaseUtil base) {
        this.base = base;
    }

    @Before
    public void initializeTest(){
        System.out.println("Opening Browser..");
        WebDriverManager.chromedriver().driverVersion("107.0.5304.107").setup();

        base.driver =  new ChromeDriver();
        WebDriverWait wait = new WebDriverWait(this.base.driver, Duration.ofSeconds(10));
        base.dropDownListPage = new DropDownListPage(this.base.driver);
        base.searchBookPage = new SearchBookPage(this.base.driver, wait);

        base.driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(40));
        base.driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(40));
        base.driver.manage().window().maximize();
    }

    @After
    public void tearDownTest() throws InterruptedException {
        Thread.sleep(2000);
        base.driver.close();
        base.driver.quit();
    }

    @Given("User go to {string}")
    public void user_go_to(String value) {
        // Write code here that turns the phrase above into concrete actions

        base.driver.navigate().to(value);
        System.out.println("--Navigate to Page--");
    }
}
