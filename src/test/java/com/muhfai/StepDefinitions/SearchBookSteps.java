package com.muhfai.StepDefinitions;

import com.muhfai.base.BaseUtil;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class SearchBookSteps extends BaseUtil{

    private BaseUtil base;

    public SearchBookSteps(BaseUtil base) {
        this.base = base;
    }

    @When("User in {string} page")
    public void userInPage(String value) {
        System.out.println("--Welcome to Book Store Page--");
        base.searchBookPage.checkSuccessLandedPage(value);
    }

    @And("User search book {string}")
    public void userSearchBook(String value) {
        System.out.println("--User search a book--");
        base.searchBookPage.searchBook(value);
    }

    @Then("User see {string}")
    public void userSee(String value) throws InterruptedException {
        if(value.equals("No rows found")){
            base.searchBookPage.checkUserSeeNotFound(value);
            System.out.println("-- User see No rows found --");
        }else{
            base.searchBookPage.checkUserSeeBook();
            System.out.println("-- User see a Book --");
        }


    }

    @And("User click book {string}")
    public void userClickBook(String value) {
        base.searchBookPage.clickBook(value);
    }


}
